// kssl_private_key.h: private key storage for the keyless ssl
//
// Copyright (c) 2013 CloudFlare, Inc.

// need to edit this file to support mbedtls 

#ifndef INCLUDED_KSSL_PRIVATE_KEY
#define INCLUDED_KSSL_PRIVATE_KEY 1
#include <unistd.h>
#include "mbedtls/pk.h"


#define BYTE uint8_t
#define WORD uint16_t
#define DWORD uint32_t
#define KSSL_SKI_SIZE 20
#define KSSL_DIGEST_SIZE 32


//#include "/tmp/TaLoS/src/libressl-2.4.1/include/openssl/evp.h"
//#include "../untrusted/kssl_log.h"

// public definition of private key list
// private_key is an EVP key with its associate SHA256 ski
typedef struct {
  BYTE ski[KSSL_SKI_SIZE];         // SKI of public key.
  BYTE digest[KSSL_DIGEST_SIZE];   // SHA256 digest of key.
  //EVP_PKEY *key;                   // EVP private key
  mbedtls_pk_context *key;
} private_key;

// pk_list_ is an array of private_key structures
struct pk_list_ {
  int current;           // Number of entries in privates
  int allocated;         // Size of the privates array
  private_key *privates; // Array of private_key
};
typedef struct pk_list_* pk_list;


// interface for private key list

// new_pk_list: initializes an array of private keys. Returns a
// pointer to an opaque structure. count is the number of private keys
// to allocate space for.



#endif // INCLUDED_KSSL_PRIVATE_KEY
