//#include <openssl/x509.h>
#include "../untrusted/kssl.h"

#include "kssl_private_key.h"
#include "key_operations.h"
#include "key_operations_t.h"  /* print_string */




#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#define mbedtls_printf     printf
#define mbedtls_exit       exit
#define MBEDTLS_EXIT_SUCCESS EXIT_SUCCESS
#define MBEDTLS_EXIT_FAILURE EXIT_FAILURE
#endif

#if defined(MBEDTLS_BIGNUM_C) && defined(MBEDTLS_RSA_C) && \
    defined(MBEDTLS_FS_IO) && defined(MBEDTLS_ENTROPY_C) && \
    defined(MBEDTLS_CTR_DRBG_C)


#include <string.h>

#endif



#include "mbedtls/error.h"
#include "mbedtls/pk.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/config.h"
#include "mbedtls/rsa.h"
#include"mbedtls/md.h"

//#define mbedtls_printf     printf

extern int silent;
mbedtls_rsa_context RSAA;



//#define RSA_NO_PADDING		3
//#define RSA_PKCS1_PADDING	1
typedef struct mbedtls_rsa_context RSA;
typedef struct mbedtls_ecdh_context EC_KEY;
typedef struct pk_list_* pk_list;
#define RSA_PKCS1_PADDING	1
#define RSA_SSLV23_PADDING	2
#define RSA_NO_PADDING		3
#define RSA_PKCS1_OAEP_PADDING	4
int ecall_key_operations_sample(           // Opcode from a KSSL message indicating the operation
int length,           // Length of data in message
BYTE *message,        // Bytes to perform operation on
BYTE *out,            // Buffer into which operation output is written
unsigned int *size,
unsigned int max_payload_size) { // Size of returned data written here

      mbedtls_entropy_context entropy;
      mbedtls_ctr_drbg_context ctr_drbg;
     // unsigned char result[1024];
      unsigned char buf[512];
      int ret = 1;
      int return_val = 1;
      size_t i;
	    int c = 1;	
      const char *pers = "rsa_decrypt";


      mbedtls_rsa_context rsa;
      mbedtls_rsa_init(&rsa, MBEDTLS_RSA_PKCS_V15, 0 );
      mbedtls_ctr_drbg_init( &ctr_drbg );
      mbedtls_entropy_init( &entropy );
//ocall_check(message, length);
      ret = mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func,
                                        &entropy, (const unsigned char *) pers, 11);
                                       // strlen( pers ) );



      if( ret != 0 )
        {
           
        }


    if( ( return_val = mbedtls_mpi_read_string( &rsa.N , 16, "A1D46FBA2318F8DCEF16C280948B1CF27966B9B47225ED2989F8D74B45BD36049C0AAB5AD0FF003553BA843C8E12782FC5873BB89A3DC84B883D25666CD22BF3ACD5B675969F8BEBFBCAC93FDD927C7442B178B10D1DFF9398E52316AAE0AF74E594650BDC3C670241D418684593CDA1A7B9DC4F20D2FDC6F66344074003E211" ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.E , 16, "010001" ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.D , 16, "589552BB4F2F023ADDDD5586D0C8FD857512D82080436678D07F984A29D892D31F1F7000FC5A39A0F73E27D885E47249A4148C8A5653EF69F91F8F736BA9F84841C2D99CD8C24DE8B72B5C9BE0EDBE23F93D731749FEA9CFB4A48DD2B7F35A2703E74AA2D4DB7DE9CEEA7D763AF0ADA7AC176C4E9A22C4CDA65CEC0C65964401" ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.P , 16, "CD083568D2D46C44C40C1FA0101AF2155E59C70B08423112AF0C1202514BBA5210765E29FF13036F56C7495894D80CF8C3BAEE2839BACBB0B86F6A2965F60DB1") ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.Q , 16, "CA0EEEA5E710E8E9811A6B846399420E3AE4A4C16647E426DDF8BBBCB11CD3F35CE2E4B6BCAD07AE2C0EC2ECBFCC601B207CDD77B5673E16382B1130BF465261" ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.DP, 16, "0D0E21C07BF434B4A83B116472C2147A11D8EB98A33CFBBCF1D275EF19D815941622435AAF3839B6C432CA53CE9E772CFBE1923A937A766FD93E96E6EDEC1DF1" ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.DQ, 16, "269CEBE6305DFEE4809377F078C814E37B45AE6677114DFC4F76F5097E1F3031D592567AC55B9B98213B40ECD54A4D2361F5FAACA1B1F51F71E4690893C4F081") ) != 0 ||
        ( return_val = mbedtls_mpi_read_string( &rsa.QP, 16, "97AC5BB885ABCA314375E9E4DB1BA4B2218C90619F61BD474F5785075ECA81750A735199A8C191FE2D3355E7CF601A70E5CABDE0E02C2538BB9FB4871540B3C1" ) ) != 0 )
    {
        //exit_val = MBEDTLS_EXIT_FAILURE;
     //   mbedtls_printf( " failed\n  ! mbedtls_mpi_read_file returned %d\n\n",
     //                   return_val);
  //      fclose( f );
  	//goto exit;
    }


    rsa.len = ( mbedtls_mpi_bitlen( &rsa.N ) + 7 ) >> 3;
    RSAA = rsa;




     if (opcode == KSSL_OP_RSA_DECRYPT || opcode == KSSL_OP_RSA_DECRYPT_RAW) {

      
     //       mbedtls_printf( "\n  . Decrypting the encrypted data" );
       //     fflush( stdout );

            return_val = mbedtls_rsa_pkcs1_decrypt( &rsa, mbedtls_ctr_drbg_random,
                                                    &ctr_drbg, MBEDTLS_RSA_PRIVATE, &i,
                                                    message, out, 1024 );
          *size = (unsigned int)i;


	//   mbedtls_printf("%s\n",out);
            if( return_val != 0 )
            {
                //exit_val = MBEDTLS_EXIT_FAILURE;
        //        mbedtls_printf( " failed\n  ! mbedtls_rsa_pkcs1_decrypt returned %d\n\n",
        //                        return_val );
                //goto exit;
            }

    } else if () {
    digest_nid = opcode_to_digest_nid(opcode);
    //RSA signature
    if (KSSL_OP_RSA_SIGN_MD5SHA1 <= opcode && opcode <= KSSL_OP_RSA_SIGN_SHA512) {
      rsa = EVP_PKEY_get1_RSA(list->privates[key_id].key);
      if (rsa == NULL) {
        ERR_clear_error();
        return KSSL_ERROR_CRYPTO_FAILED;
      }
      if (RSA_sign(digest_nid, message, length, out, size, rsa) != 1) {
        ERR_clear_error();
        return KSSL_ERROR_CRYPTO_FAILED;
      }
    }


      //mbedtls_printf(mbedtls_pk_get_type(rsa));

          /*if (rsa == NULL) {
            ERR_clear_error();
            return KSSL_ERROR_CRYPTO_FAILED;
          } */

       // padding = (opcode == KSSL_OP_RSA_DECRYPT_RAW ? RSA_NO_PADDING : RSA_PKCS1_PADDING);
        //s = RSA_private_decrypt(length, message, out, rsa, padding);

      //ret = mbedtls_rsa_pkcs1_decrypt(rsa, mbedtls_ctr_drbg_random, &ctr_drbg, MBEDTLS_RSA_PRIVATE, (size_t *)size , message, out,(size_t)size);
      //write_log(0, "plain text: %s", message);
         //rite_log(0, "priv keys loaded1: %s", out);

      //   if ((ret = mbedtls_rsa_pkcs1_decrypt(rsa, mbedtls_ctr_drbg_random, &ctr_drbg, MBEDTLS_RSA_PRIVATE, (size_t *)size , buf, out,(size_t)size) ) != 0)
      //   {
      //     mbedtls_printf( " failed\n  ! mbedtls_rsa_pkcs1_decrypt returned %d\n\n", ret );
      // printf("my number is 0x%02X\n",ret);
      //   }

        // if (s != -1) {
        //   *size = (unsigned int)s;
        // } else {
        //   ERR_clear_error();
        //   return KSSL_ERROR_CRYPTO_FAILED;
        // }
     // } else {
        //digest_nid = opcode_to_digest_nid(opcode);
        //RSA signature
       /* if (KSSL_OP_RSA_SIGN_MD5SHA1 <= opcode && opcode <= KSSL_OP_RSA_SIGN_SHA512) {
	unsigned int hashlen =256;
            unsigned char hash[length];
   // unsigned char buf[MBEDTLS_MPI_MAX_SIZE];
        
         //rsa = mbedtls_pk_rsa(*(list->privates[key_id].key));


      //    mbedtls_printf( "\n  . Generating the RSA/SHA-256 signature" );


         if( ( ret = mbedtls_md(mbedtls_md_info_from_type( MBEDTLS_MD_SHA256 ), message, length,
                   hash)) != 0 )
          {
        //      mbedtls_printf( " failed\n  ! Could not open or read\n\n" );
             // goto exit;
          }
         

          if( ( ret = mbedtls_rsa_pkcs1_sign(&rsa, NULL, NULL, MBEDTLS_RSA_PRIVATE, MBEDTLS_MD_SHA256,
                                    hashlen, hash, out ) ) != 0 )
             {
          //     mbedtls_printf( " failed\n  ! mbedtls_rsa_pkcs1_sign returned -0x%0x\n\n", -ret );
              // goto exit;
             }
          *size = hashlen;
          // if (RSA_sign(digest_nid, message, length, out, size, rsa) != 1) {
          //   ERR_clear_error();
          //   return KSSL_ERROR_CRYPTO_FAILED;
          // }

        }*/ 
        // to do ecdh
        // else if (KSSL_OP_ECDSA_SIGN_MD5SHA1 <= opcode && opcode <= KSSL_OP_ECDSA_SIGN_SHA512) {
        //   //ECDSA signature
        //   ec_key = EVP_PKEY_get1_EC_KEY(list->privates[key_id].key);
        //   if (ec_key == NULL) {
        //     ERR_clear_error();
        //     return KSSL_ERROR_CRYPTO_FAILED;
        //   }
        //   if (ECDSA_sign(digest_nid, message, length, out, size, ec_key) != 1) {
        //     ERR_clear_error();
        //     return KSSL_ERROR_CRYPTO_FAILED;
        //   }
        // } 
     
      

      return KSSL_ERROR_NONE;
}


