#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <pwd.h>
#include <libgen.h>
#include <stdlib.h>

#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>


# define MAX_PATH FILENAME_MAX

#include <sgx_urts.h>

#include "sample.h"
#include "kssl.h"

#include "kssl.h"
#include "kssl_helpers.h"

#include "kssl_private_key.h"
#include "kssl_core.h"
#include "kssl_log.h"

#include "key_operations_u.h"
#include "sgx_error.h"       /* sgx_status_t */
#include "sgx_eid.h"     /* sgx_enclave_id_t */


// Helper macros for stream processing. These macros ensure that the correct
// byte ordering is used.

// b is the buffer to read from/write to
// o is the offset in the buffer, incremented after the read/write
// v is the value to set
#define READ_BYTE(b, o) (b)[(o)]; (o)++;
#define READ_WORD(b, o) ntohs(*(WORD*)(&(b)[(o)])); (o) += sizeof(WORD);
#define READ_DWORD(b, o) ntohl(*(DWORD*)(&(b)[(o)])); (o) += sizeof(DWORD);
#define WRITE_BYTE(b, o, v) (b)[(o)] = (v); (o)++;
#define WRITE_WORD(b, o, v) *(WORD*)(&(b)[(o)]) = htons((v)); (o) += sizeof(WORD);
#define WRITE_DWORD(b, o, v) *(DWORD*)(&(b)[(o)]) = htonl((v)); (o) += sizeof(DWORD);
#define WRITE_BUFFER(b, o, v, l) memcpy(&(b)[(o)], (v), (l)); (o) += l;

#if PLATFORM_WINDOWS
#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif
#ifndef INET_ADDRSTRLEN
#define INET_ADDRSTRLEN 16
#endif
#define PRINT_IP InetNtop
#else
#define PRINT_IP inet_ntop
#endif
extern sgx_enclave_id_t global_eid;    /* global enclave id */
/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;
extern int silent;

typedef struct _sgx_errlist_t {
    sgx_status_t err;
    const char *msg;
    const char *sug; /* Suggestion */
} sgx_errlist_t;



/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
    {
        SGX_ERROR_UNEXPECTED,
        "Unexpected error occurred.",
        NULL
    },
    {
        SGX_ERROR_INVALID_PARAMETER,
        "Invalid parameter.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_MEMORY,
        "Out of memory.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_LOST,
        "Power transition occurred.",
        "Please refer to the sample \"PowerTransition\" for details."
    },
    {
        SGX_ERROR_INVALID_ENCLAVE,
        "Invalid enclave image.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ENCLAVE_ID,
        "Invalid enclave identification.",
        NULL
    },
    {
        SGX_ERROR_INVALID_SIGNATURE,
        "Invalid enclave signature.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_EPC,
        "Out of EPC memory.",
        NULL
    },
    {
        SGX_ERROR_NO_DEVICE,
        "Invalid SGX device.",
        "Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
    },
    {
        SGX_ERROR_MEMORY_MAP_CONFLICT,
        "Memory map conflicted.",
        NULL
    },
    {
        SGX_ERROR_INVALID_METADATA,
        "Invalid enclave metadata.",
        NULL
    },
    {
        SGX_ERROR_DEVICE_BUSY,
        "SGX device was busy.",
        NULL
    },
    {
        SGX_ERROR_INVALID_VERSION,
        "Enclave version was invalid.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ATTRIBUTE,
        "Enclave was not authorized.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_FILE_ACCESS,
        "Can't open enclave file.",
        NULL
    },
};
/* Check error conditions for loading enclave */
void print_error_message(sgx_status_t ret)
{
    size_t idx = 0;
    size_t ttl = sizeof sgx_errlist/sizeof sgx_errlist[0];

    for (idx = 0; idx < ttl; idx++) {
        if(ret == sgx_errlist[idx].err) {
            if(NULL != sgx_errlist[idx].sug)
                printf("Info: %s\n", sgx_errlist[idx].sug);
            printf("Error: %s\n", sgx_errlist[idx].msg);
            break;
        }
    }

    if (idx == ttl)
        printf("Error: Unexpected error occurred.\n");
}

// key_size: returns the size of an RSA key in bytes
int key_size(pk_list list,  // Array of private keys from new_pk_list
             int key_id) {  // ID of key from find_private_key
  //return EVP_PKEY_size(list->privates[key_id].key);
 //mbedtls_printf("size key  %d\n",  (int)mbedtls_pk_get_bitlen(list->privates[key_id].key));
 // return RSAA.len;
return 128;
//return 49;

}

/* Initialize the enclave:
 *   Step 1: retrive the launch token saved by last transaction
 *   Step 2: call sgx_create_enclave to initialize an enclave instance
 *   Step 3: save the launch token if it is updated
 */
int initialize_enclave(void)
{
    char token_path[MAX_PATH] = {'\0'};
    sgx_launch_token_t token = {0};
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    int updated = 0;
    /* Step 1: retrive the launch token saved by last transaction */

    /* try to get the token saved in $HOME */
    const char *home_dir = getpwuid(getuid())->pw_dir;
    if (home_dir != NULL && 
        (strlen(home_dir)+strlen("/")+sizeof(TOKEN_FILENAME)+1) <= MAX_PATH) {
        /* compose the token path */
        strncpy(token_path, home_dir, strlen(home_dir));
        strncat(token_path, "/", strlen("/"));
        strncat(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME)+1);
    } else {
        /* if token path is too long or $HOME is NULL */
        strncpy(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME));
    }

    FILE *fp = fopen(token_path, "rb");
    if (fp == NULL && (fp = fopen(token_path, "wb")) == NULL) {
        printf("Warning: Failed to create/open the launch token file \"%s\".\n", token_path);
    }
    printf("token_path: %s\n", token_path);
    if (fp != NULL) {
        /* read the token from saved file */
        size_t read_num = fread(token, 1, sizeof(sgx_launch_token_t), fp);
        if (read_num != 0 && read_num != sizeof(sgx_launch_token_t)) {
            /* if token is invalid, clear the buffer */
            memset(&token, 0x0, sizeof(sgx_launch_token_t));
            printf("Warning: Invalid launch token read from \"%s\".\n", token_path);
        }
    }

    /* Step 2: call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */

    ret = sgx_create_enclave(KEY_OPERATIONS_FILENAME, SGX_DEBUG_FLAG, &token, &updated, &global_eid, NULL);

    if (ret != SGX_SUCCESS) {
        if (fp != NULL) fclose(fp);

        return -1;
    }

    /* Step 3: save the launch token if it is updated */

    if (updated == FALSE || fp == NULL) {
        /* if the token is not updated, or file handler is invalid, do not perform saving */
        if (fp != NULL) fclose(fp);
        return 0;
    }

    /* reopen the file with write capablity */
    fp = freopen(token_path, "wb", fp);
    if (fp == NULL) return 0;
    size_t write_num = fwrite(token, 1, sizeof(sgx_launch_token_t), fp);
    if (write_num != sizeof(sgx_launch_token_t))
        printf("Warning: Failed to save launch token to \"%s\".\n", token_path);
    fclose(fp);

    return 0;
}





void ocall_check(BYTE* message, int len) {
printf("messeage i printed\n");
printf("length is %d \n", len);
}
// parse_header: populates a kssl_header structure from a byte stream. Returns
// KSSL_ERROR_NONE if successful.
kssl_error_code parse_header(BYTE *bytes,            // Stream of bytes
                                                     // containing a
                                                     // kssl_header
                             kssl_header *header) {  // Returns the populated
                                                     // header (must be
                                                     // allocated by caller)
  int offset = 0;

  if (bytes == NULL || header == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  header->version_maj = READ_BYTE(bytes, offset);
  header->version_min = READ_BYTE(bytes, offset);
  header->length = READ_WORD(bytes, offset);
  header->id = READ_DWORD(bytes, offset);

  return KSSL_ERROR_NONE;
}

// parse_item: Parse a kssl_item out of the body of a KSSL message
// NOTE: The payload for the item is not copied, a reference
// to the original stream is added to the kssl_item struct. The offset
// is updated if provided. Returns KSSL_ERROR_NONE if successful.
kssl_error_code parse_item(BYTE *bytes,       // Byte stream to parse
                                              // kssl_item from
                           int *offset,       // (optional) if present
                                              // specifies offset into bytes.
                           kssl_item *item) { // The kssl_item parsed (must be
                                              // allocated by caller)
  int local_offset = 0;
  BYTE local_tag;
  WORD local_len;
  BYTE *local_data;

  if (bytes == NULL || item == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  if (offset != NULL) {
    local_offset = *offset;
  }

  local_tag = READ_BYTE(bytes, local_offset);
  local_len = READ_WORD(bytes, local_offset);
  local_data = &bytes[local_offset];
  local_offset += local_len;

  item->tag = local_tag;
  item->length = local_len;
  item->data = local_data;

  if (offset != NULL) {
    *offset = local_offset;
  }

  return KSSL_ERROR_NONE;
}

// flatten_header: serialize a header into a pre-allocated byte array
// at a given offset. The offset is updated as bytes are written.  If
// offset pointer is NULL this function starts at offset 0.
kssl_error_code flatten_header(kssl_header *header, // Pointer to kssl_header
                                                    // to serialize
                               BYTE *bytes,         // Byte buffer to write
                                                    // into (must be allocated
                                                    // and have sufficient
                                                    // space for a
                                                    // kssl_header)
                               int *offset) {       // (optional) offset into
                                                    // bytes to
  int local_offset = 0;
                                                    // write to
  if (bytes == NULL || header == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  if (offset != NULL) {
    local_offset = *offset;
  }

  WRITE_BYTE(bytes, local_offset, header->version_maj);
  WRITE_BYTE(bytes, local_offset, header->version_min);
  WRITE_WORD(bytes, local_offset, header->length);
  WRITE_DWORD(bytes, local_offset, header->id);

  if (offset != NULL) {
    *offset = local_offset;
  }

  return KSSL_ERROR_NONE;
}

// flatten_item_byte: serialize a kssl_item with a given tag and one
// byte payload at an offset. The offset is updated as bytes are written.
// If offset pointer is NULL this function starts at offset 0. Returns
// KSSL_ERROR_NONE if successful.
kssl_error_code flatten_item_byte(BYTE tag,      // The kssl_item's tag (see
                                                 // kssl.h)
                                  BYTE payload , // A single byte for the
                                                 // payload
                                  BYTE *bytes,   // Buffer into which
                                                 // kssl_item is written (must
                                                 // be pre-allocated and have
                                                 // room)
                                  int *offset) { // (optional) offset into
                                                 // bytes to start writing at
  int local_offset = 0;
  if (bytes == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  if (offset != NULL) {
    local_offset = *offset;
  }

  WRITE_BYTE(bytes, local_offset, tag);
  WRITE_WORD(bytes, local_offset, 1);
  WRITE_BYTE(bytes, local_offset, payload);

  if (offset != NULL) {
    *offset = local_offset;
  }

  return KSSL_ERROR_NONE;
}

// flatten_item: Serialize a single kssl_item. The offset is updated
// as bytes are written. If offset pointer is NULL this function
// starts at offset 0. Returns KSSL_ERROR_NONE if successful.
kssl_error_code flatten_item(BYTE tag,         // The kssl_item's tag (see
                                               // kssl.h)
                             BYTE *payload,    // Buffer containing the item's
                                               // payload
                             WORD payload_len, // Length of data from payload
                                               // to copy
                             BYTE *bytes,      // Buffer into which item is
                                               // serialized
                             int *offset) {    // (optional) offset into bytes
                                               // to write from
  int local_offset = 0;

  if (bytes == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  if (offset != NULL) {
    local_offset = *offset;
  }

  WRITE_BYTE(bytes, local_offset, tag);
  WRITE_WORD(bytes, local_offset, payload_len);
  if (payload_len > 0) {
    WRITE_BUFFER(bytes, local_offset, payload, payload_len);
  }

  if (offset != NULL) {
    *offset = local_offset;
  }

  return KSSL_ERROR_NONE;
}

// add_padding: adds padding bytes to a KSSL message. Assumes that the buffer
// being written to is calloced.
kssl_error_code add_padding(WORD size,      // Length of padding
                            BYTE *bytes,    // Buffer into which item is
                                            // serialized
                            int *offset) {  // (optional) offset into bytes
                                            // to write from
  int local_offset = 0;

  if (bytes == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  if (offset != NULL) {
    local_offset = *offset;
  }

  // Add the padding. This gets added even is padding_size == 0

  WRITE_BYTE(bytes, local_offset, KSSL_TAG_PADDING);
  WRITE_WORD(bytes, local_offset, size);

  if (offset != NULL) {
    *offset = local_offset;
  }

  return KSSL_ERROR_NONE;
}

// flatten_operation: serialize a kssl_operation
kssl_error_code flatten_operation(kssl_header *header,       
                                  kssl_operation *operation, 
                                  BYTE **out_operation,      
                                  int *length) {             
  int local_req_len;
  BYTE *local_req;
  int offset = 0;
  int padding_size = 0;
  if (header == NULL        ||
      operation == NULL     ||
      out_operation == NULL ||
      length == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  // Allocate response (header + opcode + response)
  local_req_len = KSSL_HEADER_SIZE;

  if (operation->is_opcode_set) {
    local_req_len += KSSL_OPCODE_ITEM_SIZE;
  }
  if (operation->is_payload_set) {
    local_req_len += KSSL_ITEM_HEADER_SIZE + operation->payload_len;
  }
  if (operation->is_ski_set) {
    local_req_len += KSSL_ITEM_HEADER_SIZE + KSSL_SKI_SIZE;
  }
  if (operation->is_digest_set) {
    local_req_len += KSSL_ITEM_HEADER_SIZE + KSSL_DIGEST_SIZE;
  }
  if (operation->is_ip_set) {
    local_req_len += KSSL_ITEM_HEADER_SIZE + operation->ip_len;
  }

  // The operation will always be padded to KSSL_PAD_TO +
  // KSSL_ITEM_HEADER_SIZE bytes

  if (local_req_len < KSSL_PAD_TO) {
    padding_size = KSSL_PAD_TO - local_req_len;
  }
  
  local_req_len += KSSL_ITEM_HEADER_SIZE + padding_size;

  // The memory is calloced here to ensure that it is all zero. This is
  // important because the padding added below is done by just adding a
  // KSSL_ITEM at the end of the message stating that it has N bytes of
  // padding. 

  local_req = (BYTE *)calloc(local_req_len, 1);
  if (local_req == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  // Override header length
  header->length = local_req_len - KSSL_HEADER_SIZE;

  flatten_header(header, local_req, &offset);
  if (operation->is_opcode_set) {
    flatten_item_byte(KSSL_TAG_OPCODE, operation->opcode, local_req, &offset);
  }
  if (operation->is_payload_set) {
    flatten_item(KSSL_TAG_PAYLOAD, operation->payload, operation->payload_len,
        local_req, &offset);
  }
  if (operation->is_ski_set) {
    flatten_item(KSSL_TAG_SKI, operation->ski, KSSL_SKI_SIZE,
        local_req, &offset);
  }
  if (operation->is_digest_set) {
    flatten_item(KSSL_TAG_DIGEST, operation->digest, KSSL_DIGEST_SIZE,
        local_req, &offset);
  }
  if (operation->is_ip_set) {
    flatten_item(KSSL_TAG_CLIENT_IP, operation->ip, operation->ip_len,
        local_req, &offset);
  }

  add_padding(padding_size, local_req, &offset);

  *out_operation = local_req;
  *length = local_req_len;

  return KSSL_ERROR_NONE;
}

// zero_operation: initialize a kssl_operation struct
void zero_operation(kssl_operation *operation) {
  if (operation != NULL) {
    operation->is_opcode_set = 0;
    operation->opcode = 0;
    operation->is_ski_set = 0;
    operation->ski = NULL;
    operation->is_digest_set = 0;
    operation->digest = NULL;
    operation->is_payload_set = 0;
    operation->payload = NULL;
    operation->payload_len = 0;
    operation->is_ip_set = 0;
    operation->ip = NULL;
    operation->ip_len = 0;
  }
}

// parse_message_payload: parse a message payload into a
// kssl_operation struct
kssl_error_code parse_message_payload(BYTE *payload,               //
                                      int len,                     //
                                      kssl_operation *operation) { //
  int offset = 0;
  kssl_item temp_item;
  if (payload == NULL || operation == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  zero_operation(operation);

  // Count number of items and validate structure
  while (offset < len) {
    if (len - offset < (int)(KSSL_ITEM_HEADER_SIZE)) {
      return KSSL_ERROR_FORMAT;
    }

    if (parse_item(payload, &offset, &temp_item) != KSSL_ERROR_NONE ||
        len < offset) {
      return KSSL_ERROR_FORMAT;
    }

    // Iterate through known tags, populating necessary values
    switch (temp_item.tag) {
      case KSSL_TAG_OPCODE:
      {
        // Skip over malformed tags
        if (temp_item.length != 1) {
          continue;
        }

        operation->opcode = temp_item.data[0];
        operation->is_opcode_set = 1;
        break;
      }
      case KSSL_TAG_SKI:
      {
        // Skip over malformed tags
        if (temp_item.length != KSSL_SKI_SIZE) continue;
        operation->ski = temp_item.data;
        operation->is_ski_set = 1;
        break;
      }
      case KSSL_TAG_DIGEST:
      {
        // Skip over malformed tags
        if (temp_item.length != KSSL_DIGEST_SIZE) continue;
        operation->digest = temp_item.data;
        operation->is_digest_set = 1;
        break;
      }
      case KSSL_TAG_PAYLOAD:
      {
        operation->payload_len = temp_item.length;
        operation->payload = temp_item.data;
        operation->is_payload_set = 1;
        break;
      }
      case KSSL_TAG_CLIENT_IP:
      {
        operation->ip_len = temp_item.length;
        operation->ip = temp_item.data;
        operation->is_ip_set = 1;
        break;
      }
      case KSSL_TAG_PADDING:
      {
        break;
      }
      default:
        break;
    }
  }

  // check to see if opcode and payload are set
  if (operation->is_opcode_set == 0 || operation->is_payload_set == 0) {
    return KSSL_ERROR_FORMAT;
  }

  return KSSL_ERROR_NONE;
}

// opstring: convert a KSSL opcode byte to a string
const char *opstring(BYTE op) {
  switch (op) {
  case KSSL_OP_ERROR:
    return "KSSL_OP_ERROR";
  case KSSL_OP_PING:
    return "KSSL_OP_PING";
  case KSSL_OP_PONG:
    return "KSSL_OP_PONG";
  case KSSL_OP_RSA_DECRYPT:
    return "KSSL_OP_RSA_DECRYPT";
  case KSSL_OP_RSA_DECRYPT_RAW:
    return "KSSL_OP_RSA_DECRYPT_RAW";
  case KSSL_OP_RESPONSE:
    return "KSSL_OP_RESPONSE";
  case KSSL_OP_RSA_SIGN_MD5SHA1:
    return "KSSL_OP_RSA_SIGN_MD5SHA1";
  case KSSL_OP_RSA_SIGN_SHA1:
    return "KSSL_OP_RSA_SIGN_SHA1";
  case KSSL_OP_RSA_SIGN_SHA224:
    return "KSSL_OP_RSA_SIGN_SHA224";
  case KSSL_OP_RSA_SIGN_SHA256:
    return "KSSL_OP_RSA_SIGN_SHA256";
  case KSSL_OP_RSA_SIGN_SHA384:
    return "KSSL_OP_RSA_SIGN_SHA384";
  case KSSL_OP_RSA_SIGN_SHA512:
    return "KSSL_OP_RSA_SIGN_SHA512";
  case KSSL_OP_ECDSA_SIGN_MD5SHA1:
    return "KSSL_OP_ECDSA_SIGN_MD5SHA1";
  case KSSL_OP_ECDSA_SIGN_SHA1:
    return "KSSL_OP_ECDSA_SIGN_SHA1";
  case KSSL_OP_ECDSA_SIGN_SHA224:
    return "KSSL_OP_ECDSA_SIGN_SHA224";
  case KSSL_OP_ECDSA_SIGN_SHA256:
    return "KSSL_OP_ECDSA_SIGN_SHA256";
  case KSSL_OP_ECDSA_SIGN_SHA384:
    return "KSSL_OP_ECDSA_SIGN_SHA384";
  case KSSL_OP_ECDSA_SIGN_SHA512:
    return "KSSL_OP_ECDSA_SIGN_SHA512";
  }
  return "UNKNOWN";
}

// errstring: convert a KSSL error to a string
const char *errstring(BYTE err) {
  switch (err) {
  case KSSL_ERROR_NONE:
    return "KSSL_ERROR_NONE";
  case KSSL_ERROR_CRYPTO_FAILED:
   return "KSSL_ERROR_CRYPTO_FAILED";
  case KSSL_ERROR_KEY_NOT_FOUND:
    return "KSSL_ERROR_KEY_NOT_FOUND";
  case KSSL_ERROR_READ:
    return "KSSL_ERROR_READ";
  case KSSL_ERROR_VERSION_MISMATCH:
    return "KSSL_ERROR_VERSION_MISMATCH";
  case KSSL_ERROR_BAD_OPCODE:
    return "KSSL_ERROR_BAD_OPCODE";
  case KSSL_ERROR_UNEXPECTED_OPCODE:
    return "KSSL_ERROR_UNEXPECTED_OPCODE";
  case KSSL_ERROR_FORMAT:
    return "KSSL_ERROR_FORMAT";
  case KSSL_ERROR_INTERNAL:
    return "KSSL_ERROR_INTERNAL";
  }
  return "UNKNOWN";
}

static void print_ip(kssl_operation *op, char *ip_string) {
  if (op == NULL) return;
  if (op->is_ip_set) {
    // IPv4 printing
    if (op->ip_len == 4) {
      struct in_addr ip;
      memcpy((void *)&ip.s_addr, op->ip, 4);
      PRINT_IP(AF_INET, &ip, ip_string, INET_ADDRSTRLEN);
    }
    if (op->ip_len == 16) {
      struct in6_addr ip;
      memcpy((void *)ip.s6_addr, op->ip, 16);
      PRINT_IP(AF_INET6, &ip, ip_string, INET6_ADDRSTRLEN);
    }
  }
}

// log_operation: write out a KSSL operation to the log
void log_operation(kssl_header *header, kssl_operation *op) {
  char ip_string[INET6_ADDRSTRLEN] = {0};

  // The \n at the end of the ctime return is chopped off here.

  time_t now = time(NULL); 
  char nowstring[32]; // ctime_r documentation says there must be
                      // room here for 26 bytes.
  ctime_r(&now, &nowstring[0]);

  // Strip the trailing \n

  nowstring[strlen(nowstring)-1] = '\0';

  print_ip(op, ip_string);
  
 // write_log(0, "version:%d.%d, id:%d, op:%s, ip <%s>, time %s",
 //   header->version_maj, header->version_min, header->id,
 //   opstring(op->opcode), ip_string, nowstring);
}

// log_error: log an error of the operation
void log_error(DWORD id, BYTE code) {
  time_t now = time(NULL); 
  char nowstring[32]; // ctime_r documentation says there must be
                      // room here for 26 bytes.
  ctime_r(&now, &nowstring[0]);

 // write_log(1, "id:%d, error:%s, time:%s",
//    id, errstring(code), nowstring);
}



/* Error code returned by sgx_create_enclave */

/* OCall functions */
void ocall_key_operations_sample(const char *str)
{
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("%s", str);
}


// we do not need a main function here the main function will be part of keyless.c

/* Application entry */
//int SGX_CDECL main(int argc, char *argv[])
//{
//    (void)(argc);
//    (void)(argv);
//
//    /* Changing dir to where the executable is.*/
//    char absolutePath [MAX_PATH];
//    char *ptr = NULL;
//
//    ptr = realpath(dirname(argv[0]),absolutePath);
//
//    if( chdir(absolutePath) != 0)
//    		abort();
//
//    /* Initialize the enclave */
//    if(initialize_enclave() < 0){
//
//        return -1;
//    }
//
//    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
//    int ecall_return = 0;
//
//    //d = (int*) &v;
//
//    ret = ecall_key_operations_sample(global_eid, &ecall_return, arr, key_id, opcode, length, message, out, size);
//    if (ret != SGX_SUCCESS)
//        abort();
//
//    if (ecall_return == 0) {
//      printf("Application ran with success\n");
//    }
//    else
//    {
//        printf("Application failed %d \n", ecall_return);
//    }
//
//    sgx_destroy_enclave(global_eid);
//
//    return ecall_return;
//}




kssl_error_code kssl_operate(kssl_header *header,
                             BYTE *payload,
                             pk_list privates,
                             BYTE **out_response,
                             int *out_response_len)
{
  kssl_error_code err = KSSL_ERROR_NONE;
  BYTE *local_resp = NULL;
  int local_resp_len = 0;

BYTE* out_payload = NULL;
//   unsigned char* out;
   sgx_status_t ret = SGX_ERROR_UNEXPECTED;






  // Parse the indices of the items out of the payload
  kssl_header out_header;
  kssl_operation request;
  kssl_operation response;
  zero_operation(&request);
  zero_operation(&response);

  *out_response = 0;
  *out_response_len = 0;

  // Extract the items from the payload
  err = parse_message_payload(payload, header->length, &request);
  if (err != KSSL_ERROR_NONE) {
    goto exit;
  }

  if (silent == 0) {
    log_operation(header, &request);
  }

  switch (request.opcode) {
    // Other side sent response, error or pong: unexpected
    case KSSL_OP_RESPONSE:
    case KSSL_OP_ERROR:
    case KSSL_OP_PONG:
    {
      err = KSSL_ERROR_UNEXPECTED_OPCODE;
      break;
    }

    // Echo is trivial, it just echos the complete state->header back
    // including the payload item
    case KSSL_OP_PING:
    {
      response.is_payload_set = 1;
      response.payload = request.payload;
      response.payload_len = request.payload_len;
      response.is_opcode_set = 1;
      response.opcode = KSSL_OP_PONG;

      break;
    }

    // Decrypt or sign the payload using the private key
    case KSSL_OP_RSA_DECRYPT:
    case KSSL_OP_RSA_DECRYPT_RAW:
    case KSSL_OP_RSA_SIGN_MD5SHA1:
    case KSSL_OP_RSA_SIGN_SHA1:
    case KSSL_OP_RSA_SIGN_SHA224:
    case KSSL_OP_RSA_SIGN_SHA256:
    case KSSL_OP_RSA_SIGN_SHA384:
    case KSSL_OP_RSA_SIGN_SHA512:
    case KSSL_OP_ECDSA_SIGN_MD5SHA1:
    case KSSL_OP_ECDSA_SIGN_SHA1:
    case KSSL_OP_ECDSA_SIGN_SHA224:
    case KSSL_OP_ECDSA_SIGN_SHA256:
    case KSSL_OP_ECDSA_SIGN_SHA384:
    case KSSL_OP_ECDSA_SIGN_SHA512:
    {
      int key_id;

      if (request.is_ski_set) {
        // Identify private key from request ski
        key_id = 0;//find_private_key(privates, request.ski, NULL);
      } else if (request.is_digest_set) {
        key_id = 0;//find_private_key(privates, NULL, request.digest);
      } else {
        err = KSSL_ERROR_FORMAT;
        break;
      }
      if (key_id < 0) {
        err = KSSL_ERROR_KEY_NOT_FOUND;
        break;
      }

   unsigned int max_payload_size;
   unsigned int  payload_size = 0;
   unsigned char* out;
   //sgx_status_t ret = SGX_ERROR_UNEXPECTED;
   int  payload_len = 1024;
unsigned char* payload_enc = (unsigned char *) malloc(1024);
// check if payload_enc and request payload are working fine
   max_payload_size = 64;
   out_payload = (BYTE*) malloc(max_payload_size); //chan
   

      // Allocate buffer to hold output of private key operation
      if (out_payload == NULL) {
        err = KSSL_ERROR_INTERNAL;
        break;
      }

      // Operate on payload
////////////////////      //// trusted entry point ///////////////////////
      //private_key_operation_entry

   /* Initialize the enclave */
   if(initialize_enclave() < 0){
      return KSSL_ERROR_INTERNAL;
   }   
         //sgx_status_t ret = SGX_ERROR_UNEXPECTED;
         int ecall_return = 0;

memcpy(payload_enc, request.payload, 1024);

         // err and ret what are thesee?
         ret = ecall_key_operations_sample(global_eid, &ecall_return,  payload_len,payload_enc, out_payload, &payload_size, max_payload_size);
//         printf("response%s\n", out_payload);
         printf("request   %s\n", request.payload);
         if (ret != SGX_SUCCESS)
             abort();

     
         if (ecall_return == 0) {
        	 printf("Application ran with success\n");
         }
         else
         {
        	 printf("Application failed %d \n", ecall_return);
         }

         sgx_destroy_enclave(global_eid);
// enclave handling ends here

      if (err != KSSL_ERROR_NONE) {
        err = KSSL_ERROR_CRYPTO_FAILED;
        break;
      }

      response.is_payload_set = 1;
      response.payload        = out_payload;
      response.payload_len    = payload_size;
      response.is_opcode_set  = 1;
      response.opcode         = KSSL_OP_RESPONSE;

      break;
    }

    // This should not occur
  default:
    {
      err = KSSL_ERROR_BAD_OPCODE;
      break;
    }
  }

exit:
  if (err != KSSL_ERROR_NONE) {
    err = kssl_error(header->id, err, &local_resp, &local_resp_len);
  } else {

    // Create output header

    out_header.version_maj = KSSL_VERSION_MAJ;
    out_header.version_min = KSSL_VERSION_MIN;
    out_header.id          = header->id;

    // Note that the response in &local_resp is dynamically allocated
    // and needs to be freed

    err = flatten_operation(&out_header, &response, &local_resp,
        &local_resp_len);
  }

  free(out_payload);

  if (err == KSSL_ERROR_NONE) {
    *out_response = local_resp;
    *out_response_len = local_resp_len;
  }

  return err;
}

// see core.h
kssl_error_code kssl_error(DWORD id,
                           BYTE error,
                           BYTE **response,
                           int *response_len)
{
  kssl_header e;
  int offset = 0;
  int size = KSSL_HEADER_SIZE + KSSL_OPCODE_ITEM_SIZE + KSSL_ERROR_ITEM_SIZE;
  BYTE *resp;

  // The operation will always be padded to KSSL_PAD_TO +
  // KSSL_ITEM_HEADER_SIZE bytes

  int padding_size = 0;
  if (size < KSSL_PAD_TO) {
    padding_size = KSSL_PAD_TO - size;
  }

  if (response == NULL || response_len == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  size += padding_size + KSSL_ITEM_HEADER_SIZE;

  // The memory is calloced here to ensure that it is all zero. This is
  // important because the padding added below is done by just adding a
  // KSSL_ITEM at the end of the message stating that it has N bytes of
  // padding.
  
  resp = (BYTE *)calloc(size, 1);
  if (resp == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  e.version_maj = KSSL_VERSION_MAJ;
  e.version_min = KSSL_VERSION_MIN;
  e.length = size - KSSL_HEADER_SIZE;
  e.id = id;

  flatten_header(&e, resp, &offset);
  flatten_item_byte(KSSL_TAG_OPCODE, KSSL_OP_ERROR, resp, &offset);
  flatten_item_byte(KSSL_TAG_PAYLOAD, error, resp, &offset);
  add_padding(padding_size, resp, &offset);

  *response = resp;
  *response_len = size;

  return KSSL_ERROR_NONE;}
