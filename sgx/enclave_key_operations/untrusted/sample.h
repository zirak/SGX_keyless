#ifndef _APP_H_
#define _APP_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "../trusted/kssl_private_key.h"
#include "kssl_helpers.h"


#include "kssl.h"
#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif


# define TOKEN_FILENAME   "enclave.token"
# define KEY_OPERATIONS_FILENAME "key_operations.signed.so"



#if defined(__cplusplus)
extern "C" {
#endif

kssl_error_code kssl_operate(
    kssl_header *header,        // pointer to the incoming header
    BYTE        *payload,       // pointer to the incoming payload
    pk_list      privates,      // reference to list of private keys
    BYTE       **response,      // response to be freed by caller
    int         *response_len); // length of response

#if defined(__cplusplus)
}
#endif

#endif /* !_APP_H_ */
