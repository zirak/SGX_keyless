################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sgx/enclave_key_operations/untrusted/key_operations_u.c \
../sgx/enclave_key_operations/untrusted/keyless.c \
../sgx/enclave_key_operations/untrusted/kssl_core.c \
../sgx/enclave_key_operations/untrusted/kssl_getopt.c \
../sgx/enclave_key_operations/untrusted/kssl_helpers.c \
../sgx/enclave_key_operations/untrusted/kssl_log.c \
../sgx/enclave_key_operations/untrusted/kssl_private_key.c \
../sgx/enclave_key_operations/untrusted/kssl_thread.c \
../sgx/enclave_key_operations/untrusted/sample.c \
../sgx/enclave_key_operations/untrusted/testclient.c 

O_SRCS += \
../sgx/enclave_key_operations/untrusted/key_operations_u.o 

OBJS += \
./sgx/enclave_key_operations/untrusted/key_operations_u.o \
./sgx/enclave_key_operations/untrusted/keyless.o \
./sgx/enclave_key_operations/untrusted/kssl_core.o \
./sgx/enclave_key_operations/untrusted/kssl_getopt.o \
./sgx/enclave_key_operations/untrusted/kssl_helpers.o \
./sgx/enclave_key_operations/untrusted/kssl_log.o \
./sgx/enclave_key_operations/untrusted/kssl_private_key.o \
./sgx/enclave_key_operations/untrusted/kssl_thread.o \
./sgx/enclave_key_operations/untrusted/sample.o \
./sgx/enclave_key_operations/untrusted/testclient.o 

C_DEPS += \
./sgx/enclave_key_operations/untrusted/key_operations_u.d \
./sgx/enclave_key_operations/untrusted/keyless.d \
./sgx/enclave_key_operations/untrusted/kssl_core.d \
./sgx/enclave_key_operations/untrusted/kssl_getopt.d \
./sgx/enclave_key_operations/untrusted/kssl_helpers.d \
./sgx/enclave_key_operations/untrusted/kssl_log.d \
./sgx/enclave_key_operations/untrusted/kssl_private_key.d \
./sgx/enclave_key_operations/untrusted/kssl_thread.d \
./sgx/enclave_key_operations/untrusted/sample.d \
./sgx/enclave_key_operations/untrusted/testclient.d 


# Each subdirectory must supply rules for building sources it contributes
sgx/enclave_key_operations/untrusted/%.o: ../sgx/enclave_key_operations/untrusted/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/opt/intel/sgxsdk/include/ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


