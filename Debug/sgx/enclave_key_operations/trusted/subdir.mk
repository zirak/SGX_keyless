################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../sgx/enclave_key_operations/trusted/key_operations.c \
../sgx/enclave_key_operations/trusted/kssl_private_key.c 

OBJS += \
./sgx/enclave_key_operations/trusted/key_operations.o \
./sgx/enclave_key_operations/trusted/kssl_private_key.o 

C_DEPS += \
./sgx/enclave_key_operations/trusted/key_operations.d \
./sgx/enclave_key_operations/trusted/kssl_private_key.d 


# Each subdirectory must supply rules for building sources it contributes
sgx/enclave_key_operations/trusted/%.o: ../sgx/enclave_key_operations/trusted/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/opt/intel/sgxsdk/include/ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


