#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <pwd.h>
#include <libgen.h>
#include <stdlib.h>

#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>

#if !defined(MBEDTLS_CONFIG_FILE)
#include "mbedtls/config.h"
#else
#include MBEDTLS_CONFIG_FILE
#endif

#if defined(MBEDTLS_PLATFORM_C)
#include "mbedtls/platform.h"
#else
#include <stdio.h>
#include <stdlib.h>
#define mbedtls_printf     printf
#define mbedtls_exit       exit
#define MBEDTLS_EXIT_SUCCESS EXIT_SUCCESS
#define MBEDTLS_EXIT_FAILURE EXIT_FAILURE
#endif

#if defined(MBEDTLS_BIGNUM_C) && defined(MBEDTLS_RSA_C) && \
    defined(MBEDTLS_FS_IO) && defined(MBEDTLS_ENTROPY_C) && \
    defined(MBEDTLS_CTR_DRBG_C)


#include <string.h>

#endif


#include <openssl/evp.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "mbedtls/error.h"
#include "mbedtls/pk.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/config.h"
#include "mbedtls/rsa.h"
#include"mbedtls/md.h"
#include "mbedtls/bignum.h"
# define MAX_PATH FILENAME_MAX

#include <sgx_urts.h>

#include "sample.h"

#include "kssl.h"

#include "kssl_private_key.h"

#include "key_operations_u.h"
#include "sgx_error.h"       /* sgx_status_t */
#include "sgx_eid.h"     /* sgx_enclave_id_t */
extern sgx_enclave_id_t global_eid;    /* global enclave id */

//#include "key_operations_u.h"




/* Global EID shared by multiple threads */
sgx_enclave_id_t global_eid = 0;
extern int silent;

typedef struct _sgx_errlist_t {
    sgx_status_t err;
    const char *msg;
    const char *sug; /* Suggestion */
} sgx_errlist_t;


// Helper macros for stream processing. These macros ensure that the correct
// byte ordering is used.

// b is the buffer to read from/write to
// o is the offset in the buffer, incremented after the read/write
// v is the value to set
#define READ_BYTE(b, o) (b)[(o)]; (o)++;
#define READ_WORD(b, o) ntohs(*(WORD*)(&(b)[(o)])); (o) += sizeof(WORD);
#define READ_DWORD(b, o) ntohl(*(DWORD*)(&(b)[(o)])); (o) += sizeof(DWORD);
#define WRITE_BYTE(b, o, v) (b)[(o)] = (v); (o)++;
#define WRITE_WORD(b, o, v) *(WORD*)(&(b)[(o)]) = htons((v)); (o) += sizeof(WORD);
#define WRITE_DWORD(b, o, v) *(DWORD*)(&(b)[(o)]) = htonl((v)); (o) += sizeof(DWORD);
#define WRITE_BUFFER(b, o, v, l) memcpy(&(b)[(o)], (v), (l)); (o) += l;

#if PLATFORM_WINDOWS
#ifndef INET6_ADDRSTRLEN
#define INET6_ADDRSTRLEN 46
#endif
#ifndef INET_ADDRSTRLEN
#define INET_ADDRSTRLEN 16
#endif
#define PRINT_IP InetNtop
#else
#define PRINT_IP inet_ntop
#endif

void ocall_check(BYTE* message, int len) {
printf("messeage i printed\n");
printf("length is %d \n", len);
}




// key_size: returns the size of an RSA key in bytes
int key_size(pk_list list,  // Array of private keys from new_pk_list
             int key_id) {  // ID of key from find_private_key
  //return EVP_PKEY_size(list->privates[key_id].key);
 //mbedtls_printf("size key  %d\n",  (int)mbedtls_pk_get_bitlen(list->privates[key_id].key));
 // return RSAA.len;
return 128;
//return 49;

}

/* Error code returned by sgx_create_enclave */
static sgx_errlist_t sgx_errlist[] = {
    {
        SGX_ERROR_UNEXPECTED,
        "Unexpected error occurred.",
        NULL
    },
    {
        SGX_ERROR_INVALID_PARAMETER,
        "Invalid parameter.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_MEMORY,
        "Out of memory.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_LOST,
        "Power transition occurred.",
        "Please refer to the sample \"PowerTransition\" for details."
    },
    {
        SGX_ERROR_INVALID_ENCLAVE,
        "Invalid enclave image.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ENCLAVE_ID,
        "Invalid enclave identification.",
        NULL
    },
    {
        SGX_ERROR_INVALID_SIGNATURE,
        "Invalid enclave signature.",
        NULL
    },
    {
        SGX_ERROR_OUT_OF_EPC,
        "Out of EPC memory.",
        NULL
    },
    {
        SGX_ERROR_NO_DEVICE,
        "Invalid SGX device.",
        "Please make sure SGX module is enabled in the BIOS, and install SGX driver afterwards."
    },
    {
        SGX_ERROR_MEMORY_MAP_CONFLICT,
        "Memory map conflicted.",
        NULL
    },
    {
        SGX_ERROR_INVALID_METADATA,
        "Invalid enclave metadata.",
        NULL
    },
    {
        SGX_ERROR_DEVICE_BUSY,
        "SGX device was busy.",
        NULL
    },
    {
        SGX_ERROR_INVALID_VERSION,
        "Enclave version was invalid.",
        NULL
    },
    {
        SGX_ERROR_INVALID_ATTRIBUTE,
        "Enclave was not authorized.",
        NULL
    },
    {
        SGX_ERROR_ENCLAVE_FILE_ACCESS,
        "Can't open enclave file.",
        NULL
    },
};


/* Initialize the enclave:
 *   Step 1: retrive the launch token saved by last transaction
 *   Step 2: call sgx_create_enclave to initialize an enclave instance
 *   Step 3: save the launch token if it is updated
 */
int initialize_enclave(void)
{
    char token_path[MAX_PATH] = {'\0'};
    sgx_launch_token_t token = {0};
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    int updated = 0;
    /* Step 1: retrive the launch token saved by last transaction */

    /* try to get the token saved in $HOME */
    const char *home_dir = getpwuid(getuid())->pw_dir;
    if (home_dir != NULL && 
        (strlen(home_dir)+strlen("/")+sizeof(TOKEN_FILENAME)+1) <= MAX_PATH) {
        /* compose the token path */
        strncpy(token_path, home_dir, strlen(home_dir));
        strncat(token_path, "/", strlen("/"));
        strncat(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME)+1);
    } else {
        /* if token path is too long or $HOME is NULL */
        strncpy(token_path, TOKEN_FILENAME, sizeof(TOKEN_FILENAME));
    }

    FILE *fp = fopen(token_path, "rb");
    if (fp == NULL && (fp = fopen(token_path, "wb")) == NULL) {
        printf("Warning: Failed to create/open the launch token file \"%s\".\n", token_path);
    }
    printf("token_path: %s\n", token_path);
    if (fp != NULL) {
        /* read the token from saved file */
        size_t read_num = fread(token, 1, sizeof(sgx_launch_token_t), fp);
        if (read_num != 0 && read_num != sizeof(sgx_launch_token_t)) {
            /* if token is invalid, clear the buffer */
            memset(&token, 0x0, sizeof(sgx_launch_token_t));
            printf("Warning: Invalid launch token read from \"%s\".\n", token_path);
        }
    }

    /* Step 2: call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */

    ret = sgx_create_enclave(KEY_OPERATIONS_FILENAME, SGX_DEBUG_FLAG, &token, &updated, &global_eid, NULL);

    if (ret != SGX_SUCCESS) {
        if (fp != NULL) fclose(fp);

        return -1;
    }

    /* Step 3: save the launch token if it is updated */

    if (updated == FALSE || fp == NULL) {
        /* if the token is not updated, or file handler is invalid, do not perform saving */
        if (fp != NULL) fclose(fp);
        return 0;
    }

    /* reopen the file with write capablity */
    fp = freopen(token_path, "wb", fp);
    if (fp == NULL) return 0;
    size_t write_num = fwrite(token, 1, sizeof(sgx_launch_token_t), fp);
    if (write_num != sizeof(sgx_launch_token_t))
        printf("Warning: Failed to save launch token to \"%s\".\n", token_path);
    fclose(fp);

    return 0;
}

/* OCall functions */
void ocall_key_operations_sample(const char *str)
{
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("%s", str);
}

void kssl_op_rsa_decrypt(unsigned char* payload_enc, int * payload)
{
    
    static int count = 0;
    char kryptos2[255];
    int payload_len = 0;

    FILE *f;
    int return_val;

    mbedtls_rsa_context rsa;
    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;

    const char *pers = "rsa_encrypt";


    mbedtls_rsa_init( &rsa, MBEDTLS_RSA_PKCS_V15, 0 );
    mbedtls_ctr_drbg_init( &ctr_drbg );
    mbedtls_entropy_init( &entropy );

    return_val = mbedtls_ctr_drbg_seed( &ctr_drbg, mbedtls_entropy_func,
                                        &entropy, (const unsigned char *) pers,
                                        strlen( pers ) );
    if( return_val != 0 )
    {
       // exit_val = MBEDTLS_EXIT_FAILURE;
        mbedtls_printf( " failed\n  ! mbedtls_ctr_drbg_seed returned %d\n",
                        return_val );
       // goto exit;
    }

    mbedtls_printf( "\n  . Reading public key from rsa_pub.txt" );
    fflush( stdout );

    if( ( f = fopen( "rsa_pub.txt", "rb" ) ) == NULL )
    {
       // exit_val = MBEDTLS_EXIT_FAILURE;
        mbedtls_printf( " failed\n  ! Could not open rsa_pub.txt\n" \
                "  ! Please run rsa_genkey first\n\n" );
       // goto exit;
    }

    if( ( return_val = mbedtls_mpi_read_file( &rsa.N, 16, f ) ) != 0 ||
        ( return_val = mbedtls_mpi_read_file( &rsa.E, 16, f ) ) != 0 )
    {
       // exit_val = MBEDTLS_EXIT_FAILURE;
        mbedtls_printf( " failed\n  ! mbedtls_mpi_read_file returned %d\n\n",
                        return_val );
        fclose( f );
       // goto exit;
    }

    rsa.len = ( mbedtls_mpi_bitlen( &rsa.N ) + 7 ) >> 3;

    fclose( f );


//    zero_operation(&req);
    //fix the RSA_size
 //:wq
//   payload_enc =(BYTE *) malloc(128);
// pyaload_enc = char kryptos2[255];  
 * payload = 1024;

    sprintf(kryptos2, "%02x It was totally invisible, how's that possible?", count);
    count += 1;

     /*
     * Calculate the RSA encryption of the hash.
     */
    mbedtls_printf( "\n  . Generating the RSA encrypted value" );
    fflush( stdout );

    return_val = mbedtls_rsa_pkcs1_encrypt(&rsa, mbedtls_ctr_drbg_random,
                                          &ctr_drbg, MBEDTLS_RSA_PUBLIC,
                                          strlen( kryptos2 ), (unsigned char *)kryptos2, (unsigned char*) payload_enc);
    if( return_val != 0 )
    {
       // exit_val = MBEDTLS_EXIT_FAILURE;
        mbedtls_printf( " failed\n  ! mbedtls_rsa_pkcs1_encrypt returned %d\n\n",
                        return_val );
       // goto exit;
    }



}

// we do not need a main function he:re the main function will be part of keyless.c

// /* Application entry */
int SGX_CDECL main(int argc, char *argv[])
{
   //unsigned int payload_size;
   unsigned int max_payload_size;
   (void)(argc);
   (void)(argv);
   unsigned int  payload_size = 0;
   /* Changing dir to where the executable is.*/
   unsigned char* out;
   BYTE *out_payload = NULL;



   /* Initialize the enclave */
   if(initialize_enclave() < 0){
       return -1;
   }

   sgx_status_t ret = SGX_ERROR_UNEXPECTED;
   int ecall_return = 0;

   //d = (int*) &v;
    char* payload_enc = malloc(1024);
   int  payload_len = 0;
   kssl_op_rsa_decrypt(payload_enc, &payload_len);

   max_payload_size = 64;
   out_payload = (BYTE*) malloc(max_payload_size); //chan

   ret = ecall_key_operations_sample(global_eid, &ecall_return, payload_len, payload_enc, out_payload,&payload_size, max_payload_size);
   if (ret != SGX_SUCCESS)
       abort();
printf("response%s\n", out_payload);
   if (ecall_return == 0) {
     printf("Application ran with success\n");
   }
   else
   {
       printf("Application failed %d \n", ecall_return);
   }

   sgx_destroy_enclave(global_eid);

   return ecall_return;
}



/*
kssl_error_code kssl_operate(kssl_header *header,
                             BYTE *payload,
                             pk_list privates,
                             BYTE **out_response,
                             int *out_response_len)

    // Parse the indices of the items out of the payload

    BYTE *out_payload = NULL;
    zero_operation(&request);
    zero_operation(&response);

    *out_response = 0;
    *out_response_len = 0;

    // Extract the items from the payload
    err = parse_message_payload(payload, header->length, &request);
    if (err != KSSL_ERROR_NONE) {
      goto exit;
    }

    if (silent == 0) {
      log_operation(header, &request);
    }

    unsigned int payload_size;
    int max_payload_size;
    int key_id;
      // Allocate buffer to hold output of private key operation
      max_payload_size = 64;
      out_payload = (BYTE*) malloc(max_payload_size); //chan
      if (out_payload == NULL) {
        err = KSSL_ERROR_INTERNAL;
        break;
      }

      // Operate on payload
      //// trusted entry point
      //private_key_operation_entry

  //     Initialize the enclave 
         if(initialize_enclave() < 0){
        //should not  happen

             return KSSL_ERROR_BAD_OPCODE;
         }

         sgx_status_t ret = SGX_ERROR_UNEXPECTED;
         int ecall_return = 0;

         // err and ret what are thesee?
         ret = ecall_key_operations_sample(global_eid, &ecall_return,
                 request.payload_len, request.payload, out_payload,
                 &payload_size, max_payload_size);
        printf("response%s\n", out_payload);
          printf("request   %s\n", request.payload);
         if (ret != SGX_SUCCESS)
             abort();

         if (ecall_return == 0) {
           printf("Application ran with success\n");
         }

         if (ecall_return == 0) {
        	 printf("Application ran with success\n");
         }
         else
         {
        	 printf("Application failed %d \n", ecall_return);
         }

         sgx_destroy_enclave(global_eid);
// enclave handling ends here

      if (err != KSSL_ERROR_NONE) {
        err = KSSL_ERROR_CRYPTO_FAILED;
        break;
      }

      response.is_payload_set = 1;
      response.payload        = out_payload;
      response.payload_len    = payload_size;
      response.is_opcode_set  = 1;
      response.opcode         = KSSL_OP_RESPONSE;

      break;
    

    // This should not occur
  default:
    {
      err = KSSL_ERROR_BAD_OPCODE;
      break;
    }

exit:
  if (err != KSSL_ERROR_NONE) {
    err = kssl_error(header->id, err, &local_resp, &local_resp_len);
  } else {

    // Create output header

    out_header.version_maj = KSSL_VERSION_MAJ;
    out_header.version_min = KSSL_VERSION_MIN;
    out_header.id          = header->id;

    // Note that the response in &local_resp is dynamically allocated
    // and needs to be freed

   
  }

  return err;
}
*/
/*
// see core.h
kssl_error_code kssl_error(DWORD id,
                           BYTE error,
                           BYTE **response,
                           int *response_len)
{
  kssl_header e;
  int offset = 0;
  int size = KSSL_HEADER_SIZE + KSSL_OPCODE_ITEM_SIZE + KSSL_ERROR_ITEM_SIZE;
  BYTE *resp;

  // The operation will always be padded to KSSL_PAD_TO +
  // KSSL_ITEM_HEADER_SIZE bytes

  int padding_size = 0;
  if (size < KSSL_PAD_TO) {
    padding_size = KSSL_PAD_TO - size;
  }

  if (response == NULL || response_len == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  size += padding_size + KSSL_ITEM_HEADER_SIZE;

  // The memory is calloced here to ensure that it is all zero. This is
  // important because the padding added below is done by just adding a
  // KSSL_ITEM at the end of the message stating that it has N bytes of
  // padding.
  
  resp = (BYTE *)calloc(size, 1);
  if (resp == NULL) {
    return KSSL_ERROR_INTERNAL;
  }

  e.version_maj = KSSL_VERSION_MAJ;
  e.version_min = KSSL_VERSION_MIN;
  e.length = size - KSSL_HEADER_SIZE;
  e.id = id;

  flatten_header(&e, resp, &offset);
  flatten_item_byte(KSSL_TAG_OPCODE, KSSL_OP_ERROR, resp, &offset);
  flatten_item_byte(KSSL_TAG_PAYLOAD, error, resp, &offset);
  add_padding(padding_size, resp, &offset);

  *response = resp;
  *response_len = size;

  return KSSL_ERROR_NONE;
}
*/
