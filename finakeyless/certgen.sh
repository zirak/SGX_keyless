#!/bin/bash

openssl req -x509 -config openssl-ca.cnf -newkey rsa:4096 -sha256 -nodes -out cacert.pem -outform PEM
openssl x509 -in cacert.pem -text -noout
openssl x509 -purpose -in cacert.pem -inform PEM

openssl req -config openssl-server.cnf -newkey rsa:2048 -sha256 -nodes -out servercert.csr -outform PEM
openssl req -text -noout -verify -in servercert.csr

touch index.txt
echo '01' > serial.txt
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out servercert.pem -infiles servercert.csr
openssl x509 -in servercert.pem -text -noout

openssl req -config openssl-client.cnf -newkey rsa:2048 -sha256 -nodes -out clientcert.csr -outform PEM
openssl req -text -noout -verify -in clientcert.csr
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out clientcert.pem -infiles clientcert.csr
openssl x509 -in clientcert.pem -text -noout

openssl req -config openssl-client-ec.cnf -newkey ec:<(openssl ecparam -name secp384r1) -sha256 -nodes -out client_eccert.csr -outform PEM
openssl req -text -noout -verify -in client_eccert.csr
openssl ca -config openssl-ca.cnf -policy signing_policy -extensions signing_req -out client_eccert.pem -infiles client_eccert.csr
openssl x509 -in client_eccert.pem -text -noout

mkdir keys
cp clientkey.pem keys/rsa.key
cp client_eckey.pem keys/ec.key
cd keys/
openssl rsa -in rsa.key -pubout -out rsa.pubkey
openssl ec -in ec.key -pubout -out ec.pubkey
